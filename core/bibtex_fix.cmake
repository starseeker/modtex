file(READ ${INPUT_FILE} BIB_IN)
string(REPLACE "#include \"cpascal.h\"" "#include \"cpascal.h\"\n#include <setjmp.h>\njmp_buf jmp9998, jmp32; int lab31=0;" BIB_IN "${BIB_IN}")
string(REPLACE "goto lab31 ;" "{lab31=1; return;}" BIB_IN "${BIB_IN}")
string(REPLACE "goto lab32" "longjmp(jmp32,1)" BIB_IN "${BIB_IN}")
string(REPLACE "goto lab9998" "longjmp(jmp9998,1)" BIB_IN "${BIB_IN}")
string(REPLACE "lab31:" "" BIB_IN "${BIB_IN}")
string(REPLACE "lab32:" "" BIB_IN "${BIB_IN}")
string(REPLACE "hack0 () ;" "if(setjmp(jmp9998)==1) goto lab9998;" BIB_IN "${BIB_IN}")
string(REPLACE "hack1 () ;" "if(setjmp(jmp32)==0)for(;;)" BIB_IN "${BIB_IN}")
string(REPLACE "hack2 ()" "break" BIB_IN "${BIB_IN}")
string(REPLACE "while (lab31==0 )" "while ( true )"  BIB_IN "${BIB_IN}")
string(REGEX REPLACE "(.*void mainbody).*" "\\1"  FIRST_HALF "${BIB_IN}")
string(REPLACE "${FIRST_HALF}" "" SECOND_HALF "${BIB_IN}")
string(REPLACE "while ( true )" "while (lab31==0 )"  SECOND_HALF "${SECOND_HALF}")
file(WRITE bibtex.c.pre-fixwrites "${FIRST_HALF}${SECOND_HALF}")

# Local Variables:
# tab-width: 8
# mode: cmake
# indent-tabs-mode: t
# End:
# ex: shiftwidth=2 tabstop=8

