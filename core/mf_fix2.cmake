file(READ ${INPUT_FILE} MF_IN)
string(REPLACE "else write" "else\nwrite" MF_IN "${MF_IN}")
string(REPLACE " maxcoef" " lmaxcoef" MF_IN "${MF_IN}")
# Rather than try for an elaborate regex to avoid these, in the
# above replacement, just manually put them back
string(REPLACE "if ( lmaxcoef ( r ) + lmaxcoef ( v ) < 626349397L )" "if ( maxcoef ( r ) + maxcoef ( v ) < 626349397L )" MF_IN "${MF_IN}")
string(REPLACE "if ( abvscd ( lmaxcoef ( q ) , abs ( v ) , 626349396L , 65536L ) >= 0 )" "if ( abvscd ( maxcoef ( q ) , abs ( v ) , 626349396L , 65536L ) >= 0 )" MF_IN "${MF_IN}")
string(REPLACE "if ( abvscd ( lmaxcoef ( q ) , 65536L , 626349396L , abs ( v ) ) >= 0 )" "if ( abvscd ( maxcoef ( q ) , 65536L , 626349396L , abs ( v ) ) >= 0 )" MF_IN "${MF_IN}")
string(REPLACE " b1" " lb1" MF_IN "${MF_IN}")
string(REPLACE " b2" " lb2" MF_IN "${MF_IN}")
string(REPLACE " b3" " lb3" MF_IN "${MF_IN}")
file(WRITE mf.c.pre-fixwrites "${MF_IN}")

# Local Variables:
# tab-width: 8
# mode: cmake
# indent-tabs-mode: t
# End:
# ex: shiftwidth=2 tabstop=8

