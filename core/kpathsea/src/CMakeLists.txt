## Put tex-file.c first, because it's what depends on the paths, and may
## reduce frustration if the paths are wrong by doing it first.
set(libkpathsea_SOURCES
  tex-file.c
  absolute.c
  atou.c
  cnf.c
  concat.c
  concat3.c
  concatn.c
  db.c
  debug.c
  dir.c
  elt-dirs.c
  expand.c
  extend-fname.c
  file-p.c
  find-suffix.c
  fn.c
  fontmap.c
  hash.c
  kdefault.c
  kpathsea.c
  line.c
  magstep.c
  make-suffix.c
  path-elt.c
  pathsearch.c
  proginit.c
  progname.c
  readable.c
  rm-suffix.c
  str-list.c
  str-llist.c
  tex-glyph.c
  tex-hush.c
  tex-make.c
  tilde.c
  uppercasify.c
  variable.c
  version.c
  xbasename.c
  xcalloc.c
  xdirname.c
  xfopen.c
  xfseek.c
  xftell.c
  xgetcwd.c
  xmalloc.c
  xopendir.c
  xputenv.c
  xrealloc.c
  xstat.c
  xstrdup.c
  )

if(MINGW)
  set(libkpathsea_SOURCES ${libkpathsea_SOURCES} getopt.c getopt1.c)
endif(MINGW)

if(WIN32)
  if(MINGW)
    set(libkpathsea_SOURCES ${libkpathsea_SOURCES} mingw32.c xfseeko.c xftello.c)
  else(MINGW)
    set(libkpathsea_SOURCES ${libkpathsea_SOURCES} win32lib.c)
  endif(MINGW)
  set(libkpathsea_SOURCES ${libkpathsea_SOURCES} knj.c)
  add_subdirectory(win32)
else(WIN32)
  set(libkpathsea_SOURCES ${libkpathsea_SOURCES} xfseeko.c xftello.c)
endif(WIN32)

add_library(kpathsea ${libkpathsea_SOURCES})
add_dependencies(kpathsea cnf_to_paths)

## Similarly we don't want to rewrite kpathsea.h when only Makefile has
## been remade but kpathsea.h remains the same.
##
#kpathsea.h: stamp-kpathsea
#stamp-kpathsea: Makefile paths.h
#	$(AM_V_GEN)rm -f $@; \
#	( echo '/* This is a generated file */'; \
#	  echo '/* collecting all public kpathsea headers. */'; \
#	  for f in config.h c-auto.h paths.h $(normal_headers); do \
#	    echo "#include <kpathsea/$$f>"; \
#	  done ) >kpathsea.tmp
#	@if cmp -s kpathsea.h kpathsea.tmp 2>/dev/null; then \
#	  echo "kpathsea.h is unchanged"; \
#	else \
#	  echo "cp kpathsea.tmp kpathsea.h"; \
#	  cp kpathsea.tmp kpathsea.h; \
#	fi
#	rm -f kpathsea.tmp
#	date >$@

add_executable(kpseaccess access.c)
add_executable(kpsereadlink readlink.c)
add_executable(kpsestat kpsestat.c)
add_executable(kpsewhich kpsewhich.c)
target_link_libraries(kpsewhich kpathsea)

# Local Variables:
# tab-width: 8
# mode: cmake
# indent-tabs-mode: t
# End:
# ex: shiftwidth=2 tabstop=8
