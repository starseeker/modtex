# cnf-to-paths.cmake - convert texmf.cnf assignments to paths.h #define's.
# Public domain.  Translation of awk script originally written 2011, Karl Berry.

file(READ ${CMAKE_CURRENT_SOURCE_DIR}/texmf.cnf TEXMF_CNF)
string(REPLACE "\\\n" "" TEXMF_CNF "${TEXMF_CNF}")
file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/texmf.tmp "${TEXMF_CNF}")
file(STRINGS ${CMAKE_CURRENT_BINARY_DIR}/texmf.tmp TEXMF_LINES)
file(REMOVE  ${CMAKE_CURRENT_BINARY_DIR}/texmf.tmp)
file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/include/kpathsea)
file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/include/kpathsea/paths.h "/* paths.h: Generated from texmf.cnf. */\n")

# we only care about definitions with a valid C identifier (e.g.,
# TEXINPUTS, not TEXINPUTS.latex), that is, lines that look like this:
#   <name> = <value>
# (whitespace around the = is optional)

foreach(line ${TEXMF_LINES})

  # Extract the identifier and the value from the line
  string(REGEX REPLACE "%.*" "" line "${line}")
  if ("${line}" MATCHES "^[ \t]*[A-Z0-9_]+[ \t]*=")
    string(REGEX REPLACE "[A-Z0-9_]+[ \t]*=" "" VALUE "${line}")
    string(REPLACE "${VALUE}" "" VARIABLE "${line}")
    string(REGEX REPLACE "=[ \t]*$" "" VARIABLE "${VARIABLE}")
    string(REGEX REPLACE "[ \t]+" "" VARIABLE "${VARIABLE}")
    string(REGEX REPLACE "[ \t]+" "" VALUE "${VALUE}")

    # On these lines, we distinguish three cases:                         
    # 
    # 1) definitions referring to SELFAUTO*, which we want to keep.  In
    # particular, this is how the compile-time TEXMFCNF gets defined and
    # thus how texmf.cnf gets found.
    # 
    # 2) definitions starting with a /, which we also want to keep.  Here
    # we assume a distro maintainer has changed a variable, e.g.,
    # TEXMFMAIN=/usr/share/texmf, so keep it.  (This also preserves the
    # default values for OSFONTDIR and TRFONTS, but that's ok.)
    # 
    # 3) anything else, which we want to convert to a constant /nonesuch.
    # That way, the binaries don't get changed just because we change
    # definitions in texmf.cnf.
    # 
    # The definition of DEFAULT_TEXMF (and other variables)
    # that winds up in the final paths.h will not be used.

    file(APPEND ${CMAKE_CURRENT_BINARY_DIR}/include/kpathsea/paths.h "#ifndef DEFAULT_${VARIABLE}\n")
    if("${VALUE}" MATCHES "SELFAUTO" OR "${VALUE}" MATCHES "^[/]+")
       # Replace all semicolons with colons in the SELFAUTO paths we're keeping.
       # (The path-splitting code should be changed to understand both.)
       string(REGEX REPLACE ";" ":" VALUE "${VALUE}")
       file(APPEND ${CMAKE_CURRENT_BINARY_DIR}/include/kpathsea/paths.h "#define DEFAULT_${VARIABLE} \"${VALUE}\"\n")
    else("${VALUE}" MATCHES "SELFAUTO" OR "${VALUE}" MATCHES "^[/]+")
       file(APPEND ${CMAKE_CURRENT_BINARY_DIR}/include/kpathsea/paths.h "#define DEFAULT_${VARIABLE} \"/nonesuch\"\n")
    endif("${VALUE}" MATCHES "SELFAUTO" OR "${VALUE}" MATCHES "^[/]+")
    file(APPEND ${CMAKE_CURRENT_BINARY_DIR}/include/kpathsea/paths.h "#endif\n\n")
  endif ("${line}" MATCHES "^[ \t]*[A-Z0-9_]+[ \t]*=")

endforeach(line ${TEXMF_LINES})

# Local Variables:
# tab-width: 8
# mode: cmake
# indent-tabs-mode: t
# End:
# ex: shiftwidth=2 tabstop=8

