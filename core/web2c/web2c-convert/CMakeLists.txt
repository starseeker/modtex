# Copyright (c) 2017 Clifford Yapp (CMake build files only - see other
# documentation for license information about TeX and its components.)

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Minimum required version of CMake
cmake_minimum_required(VERSION 3.0.2)

# Set CMake project name
project(WEB2CCONVERT)

include(CheckIncludeFiles)
check_include_files(assert.h HAVE_ASSERT_H)
check_include_files(dirent.h HAVE_DIRENT_H)
check_include_files(dlfcn.h HAVE_DLFCN_H)
check_include_files(errno.h HAVE_ERRNO_H)
check_include_files(inttypes.h HAVE_INTTYPES_H)
check_include_files(limits.h HAVE_LIMITS_H)
check_include_files(memory.h HAVE_MEMORY_H)
check_include_files(sys/dir.h HAVE_SYS_DIR_H)
check_include_files(ndir.h HAVE_NDIR_H)
check_include_files(sys/ndir.h HAVE_SYS_NDIR_H)
check_include_files(pwd.h HAVE_PWD_H)
check_include_files(stdint.h HAVE_STDINT_H)
check_include_files(stdlib.h HAVE_STDLIB_H)
check_include_files(strings.h HAVE_STRINGS_H)
check_include_files(string.h HAVE_STRING_H)
check_include_files(sys/param.h HAVE_SYS_PARAM_H)
check_include_files(sys/stat.h HAVE_SYS_STAT_H)
check_include_files(sys/types.h HAVE_SYS_TYPES_H)
check_include_files(unistd.h HAVE_UNISTD_H)

include(CheckFunctionExists)
check_function_exists(getcwd HAVE_GETCWD)
check_function_exists(getwd HAVE_GETWD)
check_function_exists(memcmp HAVE_MEMCMP)
check_function_exists(memcpy HAVE_MEMCPY)
check_function_exists(mkstemp HAVE_MKSTEMP)
check_function_exists(mktemp HAVE_MKTEMP)
check_function_exists(putenv HAVE_PUTENV)
check_function_exists(strcasecmp HAVE_STRCASECMP)
check_function_exists(strerror HAVE_STRERROR)
check_function_exists(strrchr HAVE_STRRCHR)
check_function_exists(strstr HAVE_STRSTR)
check_function_exists(strtol HAVE_STRTOL)

configure_file(c-auto.in ${CMAKE_CURRENT_BINARY_DIR}/kpathsea/c-auto.h)

find_package(BISON)
find_package(FLEX)

add_definitions(-DNO_C_AUTO_HEADER)

include_directories(
  ${CMAKE_CURRENT_BINARY_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${CMAKE_CURRENT_SOURCE_DIR}/..
  ${KPATHSEA_INCLUDE_DIR}
  )

BISON_TARGET(web2cParser web2c-parser.y ${CMAKE_CURRENT_BINARY_DIR}/web2c-parser.c COMPILE_FLAGS "-d -v")
FLEX_TARGET(web2cLexer web2c-lexer.l ${CMAKE_CURRENT_BINARY_DIR}/web2c-lexer.c)
ADD_FLEX_BISON_DEPENDENCY(web2cLexer web2cParser)

add_library(libweb2c STATIC kps.c)

add_executable(web2c main.c ${BISON_web2cParser_OUTPUTS} ${FLEX_web2cLexer_OUTPUTS})
target_link_libraries(web2c libweb2c)

add_executable(fixwrites fixwrites.c)
target_link_libraries(fixwrites libweb2c)

add_executable(splitup splitup.c)
target_link_libraries(splitup libweb2c)

add_executable(makecpool makecpool.c)

# Local Variables:
# tab-width: 8
# mode: cmake
# indent-tabs-mode: t
# End:
# ex: shiftwidth=2 tabstop=8

