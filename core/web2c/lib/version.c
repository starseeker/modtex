#ifndef NO_C_AUTO_HEADER
# include <w2c/c-auto.h>
#endif

/* This string is appended to all the banners and used in --version.  */
/* Public domain. */

const char *versionstring = WEB2CVERSION;
