file(READ ${INPUT_FILE} MF_IN)
string(REPLACE ".\n" "\n." MF_IN "${MF_IN}")
string(REPLACE "\n.\n This" "\n\n. This" MF_IN "${MF_IN}")
string(REPLACE "\n.\n web2c.yacc" "\n\n. web2c.yacc" MF_IN "${MF_IN}")
string(REPLACE ".hh" ".hhfield" MF_IN "${MF_IN}")
string(REPLACE ".lh" ".lhfield" MF_IN "${MF_IN}")
file(WRITE mf.web2cin "${MF_IN}")

# Local Variables:
# tab-width: 8
# mode: cmake
# indent-tabs-mode: t
# End:
# ex: shiftwidth=2 tabstop=8

